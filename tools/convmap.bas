' little X11-Basic tool to create the mapdata.dat file out of the
' original mapdata.c from the original sources.
' by Markus Hoffmann
'
OPEN "I",#1,"mapdata.c"
OPEN "O",#2,"mapdata.dat"
WHILE not eof(#1)
  LINEINPUT #1,t$
  t$=TRIM$(t$)
  IF len(t$)
    IF t$="short map_data[] = {"
      st=1
    ELSE if t$="};"
      st=0
    ELSE if st=1
      IF left$(t$,2)="/*"
      ELSE
        @process(t$)
        ' print t$
      ENDIF
    ELSE
      ' print t$
    ENDIF
  ENDIF
WEND
QUIT

PROCEDURE process(t$)
  WHILE len(t$)
    SPLIT t$,",",0,a$,t$
    a$=TRIM$(a$)
    a=VAL(a$)
    PRINT #2,mki$(a);
  WEND
RETURN

