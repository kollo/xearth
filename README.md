# Xearth live Wallpaper for Android
<img alt="Logo" src="multimedia/xearth_anim.gif" width="120" />


About Xearth live Wallpaper for Android
=======================================

Copyright (C) 2011-2018 by Markus Hoffmann et al.

Xearth displays an image of the earth, as seen from your favorite vantage point
in space (this can be your current position), correctly shaded for the current
position of the sun. By default, xearth updates the displayed image every five
minutes. The time between updates as well as many parameters can be changed
using the settings.

This is a completely rewritten port of xearth V.1.1 for UNIX. 
See also: http://hewgill.com/xearth/original/

Xearth live Wallpaper f�r Android
=================================

Copyright (C) 2011-2018 by Markus Hoffmann et al.


Xearth zeigt ein Bild der Erde von Ihrem Lieblings-Aussichtspunkt im Weltraum
aus gesehen, richtig beschattet f�r die aktuelle Position der Sonne.
Standardm��ig aktualisiert xearth das angezeigte Bild alle f�nf Minuten. Die
Zeit zwischen den Updates sowie viele Parameter k�nnen �ber die Einstellungen
ge�ndert werden.

Dies ist eine v�llig neu geschriebene Portierung von xearth V.1.1 f�r UNIX.
Siehe auch: http://hewgill.com/xearth/original/

### Download

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/en/packages/de.drhoffmannsoftware.xearth/)

### Screenshots

<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="30%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" width="30%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/tenInchScreenshots/2.png" width="49%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/tenInchScreenshots/3.png" width="49%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/tenInchScreenshots/4.png" width="49%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/tenInchScreenshots/5.png" width="49%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/tenInchScreenshots/6.png" width="49%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/tenInchScreenshots/7.png" width="49%">
</div>


### LEGAL STUFF:

Parts of the source code of the Android port of xearth are 
    Copyright (C) 2011-2015 by Markus Hoffmann.
The Sources of the Android version of xearth are completely rewritten 
code based on the original sourcecode 
    Copyright (C) 1989, 1990, 1993-1995, 1999 by Kirk Lauritz Johnson
with the overlay extension based on code 
    Copyright (C) 1997-2006 by Greg Hewgill

Xearth live Wallpaper for Android is licensed under the MIT open source
license. See the file LICENSE for details.


The authors make no representations about the suitability of this
software for any purpose. It is provided "as is" without express
or implied warranty.

THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT
OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Building the .apk file
======================

The build uses the gradle environment. A ./gradlew build should do.

 git clone git@codeberg.org:kollo/xearth.git

then do a 
  cd xearth
  ./gradlew build
(Enter passwords for the keystore)
(the compile process will take a while.)

The apk should finally be in build/outputs/apk/xearth-release.apk
